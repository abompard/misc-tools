#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab :
u"""
Publish OpenOffice/LibreOffice's presentations on the web
=========================================================

This script does not handle the whole conversion process, it's rather a
bridge between different tools, which all do their part in the conversion
process. Here's an overview of the steps involved:

#. Create or edit your presentation in OpenOffice/LibreOffice_
#. Export it to PDF
#. Use it as input for this script

   - it runs pdf2svg_ for you
   - it merges the resulting SVG files
   - it adds Inkscape layer information
   - it produces an SVG file

#. Open the SVG file with Inkscape_
#. Add the JessyInk_ extension (from the 'Extensions' menu)

   - add JessyInk_ animations or transitions if you want to

#. Save the SVG file
#. Open it in a supported browser (Firefox, Chrome, Safari, Opera)

If you use slide transitions in your presentation, they will not be exported in
the SVG file. This is currently not supported (and anyway it has to be
supported in pdf2svg_ first).

If you use animations (object that appear or disappear on click), you should
use the macro in the attached document `in this mail
<http://www.mail-archive.com/dev@graphics.openoffice.org/msg00841.html>`_.
Run it on a copy of your presentation, and it will expand all the slides
containing animations. Export the result to PDF.

.. _LibreOffice: http://www.libreoffice.org/
.. _pdf2svg: http://www.cityinthesky.co.uk/opensource/pdf2svg
.. _Inkscape: http://inkscape.org/
.. _JessyInk: http://code.google.com/p/jessyink/

Use the ``--help`` command-line argument for a quick usage help.

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later

"""


__version__ = "0.1"


import os
import sys
import optparse
import tempfile
import atexit
import shutil
import subprocess
import glob

try:
    from lxml import etree
except ImportError:
    import xml.etree.ElementTree as etree
    print "You should install the 'lxml' module for Python"


NS_SVG = "http://www.w3.org/2000/svg"
NS_XLINK = "http://www.w3.org/1999/xlink"
NS_INKSCAPE = "http://www.inkscape.org/namespaces/inkscape"

DESC = """%prog helps converting a PDF presentation (for example as exported by
OpenOffice/LibreOffice) to an SVG format that can be imported in Inkscape, to
add the JessyInk extension.
%prog requires the pdf2svg tool, which you can obtain from your
distributions's package manager or download and compile it from
http://www.cityinthesky.co.uk/opensource/pdf2svg
"""

def parse_opts():
    """Parse command-line arguments"""
    usage = "%prog input.pdf output.svg"
    parser = optparse.OptionParser(usage, description=DESC,
                                   version="%%prog %s" % __version__)
    #parser.add_option("-o", "--output", help="Output file")
    opts, args = parser.parse_args()
    if len(args) != 2:
        parser.error("Wrong number of arguments")
    if not os.path.exists(args[0]):
        parser.error("No such file: %s" % args[0])
    return opts, args[0], args[1]

def pdf2svg(input_pdf, tmpdir):
    """Run pdf2svg, or error out if it's not available"""
    try:
        subprocess.call(["pdf2svg", input_pdf,
                os.path.join(tmpdir, "slide-%d.svg"), "all"])
    except OSError:
        print  "You need to install pdf2svg."
        print ("Get it from your distribution's package manager or "
               "download and compile it from "
               "http://www.cityinthesky.co.uk/opensource/pdf2svg")
        sys.exit(1)

def make_unique(svg, prefix):
    """Prefixes IDs and references, to make them unique before merging"""
    ref_elements = ["{%s}href" % NS_XLINK, "style", "clip-path"]
    for element in svg.iter():
        if "id" in element.attrib:
            element.set("id", "%s-%s" % (prefix, element.get("id")))
        for ref in ref_elements:
            if ref in element.attrib:
                element.set(ref, element.get(ref).replace("#",
                            "#%s-" % prefix))

def import_svg(svg_in, svg):
    """Import an SVG document into another"""
    # defs
    defs = svg_in.find("{%s}defs" % NS_SVG)
    if defs is not None:
        for definition in defs:
            svg.find("{%s}defs" % NS_SVG).append(definition)
    else:
        print "no defs !"
    # layers
    for group in svg_in.findall("{%s}g" % NS_SVG):
        svg.getroot().append(group)

def make_layers(svg):
    """Add Inkscape layer information to an SVG file"""
    for index, group in enumerate(svg.findall("{%s}g" % NS_SVG)):
        group.set("{%s}groupmode" % NS_INKSCAPE, "layer")
        group.set("{%s}label" % NS_INKSCAPE, "slide %d" % (index + 1))
        if index > 0:
            group.set("style", "display:none")


def main():
    """Run the whole process"""
    _, input_pdf, output = parse_opts()
    tmpdir = tempfile.mkdtemp()
    atexit.register(shutil.rmtree, tmpdir)
    pdf2svg(input_pdf, tmpdir)
    svg_files = glob.glob(os.path.join(tmpdir, "slide-*.svg"))
    if not svg_files:
        print "ERROR: no pages found in the PDF"
        sys.exit(1)
    svg_files.sort(key=lambda f: int(os.path.basename(f)[6:-4]))
    svg = etree.parse(svg_files[0])
    for svg_in_path in svg_files[1:]:
        svg_in = etree.parse(svg_in_path)
        prefix, _ = os.path.splitext(os.path.basename(svg_in_path))
        make_unique(svg_in, prefix)
        import_svg(svg_in, svg)
    make_layers(svg)
    svg.write(output, encoding="utf-8")
    print ("Now, open %s in Inkscape, and add JessyInk to it (in the "
           "'Extensions' menu)." % output)


if __name__ == "__main__":
    main()
