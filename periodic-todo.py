#!/usr/bin/env python3
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:
"""

Periodic TODO
-------------

Creates todo itemps in a periodic schedule.

.. :Authors:
       Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
       GNU GPL v3 or later
"""

import argparse
import calendar
import datetime
import logging
import os
import os.path
import re
import sys
from email.mime.text import MIMEText
from email.utils import formatdate
from subprocess import Popen, PIPE

import requests
import yaml
from dateutil.parser import parse as date_parse


LOG_CONFIG = {
    "level": logging.WARNING,
    "format": "%(asctime)s %(message)s",
    "datefmt": "%H:%M:%S",
}
log = logging.getLogger(__name__)


class Backend:
    name = None

    def __init__(self, config=None):
        self.config = config

    def add(self, name, description=None):
        raise NotImplementedError


class Trello(Backend):
    """
    Trello backend. Requires two configuration items set:
    - to: the email address to send to
    - from: the email address to send as
    """

    name = "trello"
    api_url = "https://api.trello.com"

    def __init__(self, config=None):
        super().__init__(config)
        self._cache = {}

    def add(self, todo):
        return self.add_by_rest(todo)

    def add_by_email(self, todo):
        log_message = "Sending a mail to '{}' with subject: '{}' and ".format(
            self.config["to"], todo.name
        )
        if todo.description is None:
            log_message += "no description"
        else:
            log_message += "description: '{}'".format(todo.description)
        log.info(log_message)
        # Create a text/plain message
        msg = MIMEText(todo.description or "")
        msg["Subject"] = "{} #{}".format(todo.name, todo.period)
        msg["From"] = self.config["from"]
        msg["To"] = self.config["to"]
        msg["Date"] = formatdate()
        self.sendmail(msg)

    def sendmail(self, msg):
        """invoke sendmail program to send a message."""
        cmd = ["/usr/sbin/sendmail", msg["To"]]
        proc = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        msg_text = msg.as_string().replace("\n.\n", "\n..\n")
        out, err = proc.communicate(msg_text)
        log.debug("stdout: %s", out)
        log.debug("stderr: %s", err)
        if proc.returncode != 0:
            log.error("Sending failed! TODOs were not added")

    @property
    def board_id(self):
        if self._cache.get("board_id") is None:
            result = requests.get(
                "{}/1/search".format(self.api_url),
                {
                    "key": self.config["api_key"],
                    "token": self.config["token"],
                    "modelTypes": "boards",
                    "query": self.config["board"],
                },
            )
            boards = result.json()["boards"]
            if len(boards) == 0:
                raise AssertionError(
                    "No boards found with name: {}".format(self.config["board"])
                )
            elif len(boards) == 1:
                board = boards[0]
            else:
                for board in boards:
                    if board["idOrganization"] is None:
                        break
            self._cache["board_id"] = board["id"]
        return self._cache["board_id"]

    @property
    def list_id(self):
        if self._cache.get("list_id") is None:
            result = requests.get(
                "{}/1/boards/{}/lists".format(self.api_url, self.board_id),
                {"key": self.config["api_key"], "token": self.config["token"]},
            )
            for list_obj in result.json():
                if list_obj["name"] == self.config["list"]:
                    break
            else:
                print(result.json())
                raise AssertionError(
                    "No list found with name: {}".format(self.config["list"])
                )
            self._cache["list_id"] = list_obj["id"]
        return self._cache["list_id"]

    def get_status(self):
        cards = {}
        for status in ("open", "closed"):
            result = requests.get(
                "{}/1/lists/{}/cards/{}".format(self.api_url, self.list_id, status),
                {
                    "key": self.config["api_key"],
                    "token": self.config["token"],
                    "fields": "id,name,desc,dateLastActivity,labels",
                },
            )
            for card in result.json():
                if card["name"] in cards:
                    continue  # only keep the first one
                card["status"] = status
                if status == "closed":
                    card["done_date"] = date_parse(card["dateLastActivity"])
                cards[card["name"]] = card
        return cards

    def add_by_rest(self, todo):
        log.info("Creating %s", todo.name)
        post_data = {
            "key": self.config["api_key"],
            "token": self.config["token"],
            "name": todo.name,  # "labels": "blue",
        }
        if todo.description:
            post_data["desc"] = todo.description
        result = requests.post(
            "{}/1/lists/{}/cards".format(self.api_url, self.list_id), post_data
        )
        assert result.status_code == 200


class Todo:
    def __init__(self, name, description=None):
        self.name = name
        self.description = description
        self.period = None


class StatusManager:
    def __init__(self):
        self._status = None

    def should_be_created(self, todo):
        if todo.name not in self._status:
            return True
        if self._status[todo.name]["status"] == "open":
            return False
        done_date = self._status[todo.name]["done_date"]
        create_after = self.get_create_after(done_date, todo.period)
        if datetime.date.today() >= create_after:
            return True
        return False

    def get_create_after(self, done_date, period):
        if period == "daily":
            period = "every_1_days"
        elif period == "weekly":
            period = "every_7_days"
        elif period == "monthly":
            period = "every_1_month"

        every_re = re.compile(r"every_(\d+)_(\w+)")
        if every_re.match(period):
            mo = every_re.match(period)
            count = int(mo.group(1))
            unit = mo.group(2)
            if unit.startswith("day"):
                create_after = done_date + datetime.timedelta(days=count)
            elif unit.startswith("month"):
                days = 31 * count - int(count / 2)
                create_after = done_date + datetime.timedelta(days=days)
                create_after = create_after.replace(day=1)
            else:
                raise AssertionError("Unsupported unit: %s" % unit)
        elif period.startswith("on_"):
            day_name = period[3:].capitalize()
            day_index = list(calendar.day_name).index(day_name)
            days_delta = day_index - done_date.weekday()
            if days_delta <= 0:
                days_delta += 7
            create_after = done_date + datetime.timedelta(days=days_delta)
        return create_after.date()

    def refresh(self, backend):
        self._status = backend.get_status()


def get_backend(configuration):
    for backend_class in Backend.__subclasses__():
        if backend_class.name == configuration["backend"]:
            backend = backend_class(configuration.get("backend-config"))
            break
    else:
        log.error("Unknown backend: %s", configuration["backend"])
        sys.exit(1)
    return backend


def parse_args():
    parser = argparse.ArgumentParser(description="Create TODO items periodically.")
    parser.add_argument(
        "-c",
        "--config",
        default=os.path.expanduser("~/.config/periodic-todo.yaml"),
        help="path to the configuration file",
    )
    parser.add_argument(
        "--verbose", action="store_true", help="output more information"
    )
    parser.add_argument(
        "--debug", action="store_true", help="output a lot more information"
    )
    args = parser.parse_args()
    if not os.path.exists(args.config):
        parser.error("no such configuration file: {}".format(args.config))
    return args


def main():
    args = parse_args()
    if args.debug:
        LOG_CONFIG["level"] = logging.DEBUG
    elif args.verbose:
        LOG_CONFIG["level"] = logging.INFO
    logging.basicConfig(**LOG_CONFIG)
    logging.getLogger("requests").setLevel(logging.WARNING)

    with open(args.config) as config_file:
        configuration = yaml.load(config_file.read())

    backend = get_backend(configuration)
    status_manager = StatusManager()
    status_manager.refresh(backend)

    for period, todos in configuration.get("todo", {}).items():
        for item in todos:
            if isinstance(item, str):
                todo = Todo(item)
            elif isinstance(item, dict):
                todo = Todo(item["name"], item.get("description"))
            else:
                log.warning("Invalid todo format: %r", item)
            todo.period = period
            if status_manager.should_be_created(todo):
                backend.add(todo)


if __name__ == "__main__":
    main()
