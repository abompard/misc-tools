#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:
u"""

Birthdays from VCard
--------------------

Creates an ICalendar file (ICS) from the birthdays in a VCard file (VCF).

.. :Authors:
       Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
       GNU GPL v3 or later
"""

import sys
import os, vobject, datetime, time
import re


#event_tpl = """\r
#BEGIN:VEVENT\r
#UID:%(uid)s\r
#SUMMARY:Anniv. %(name)s\r
#CATEGORIES:Anniversaire\r
#DTSTART:%(date)sT060000Z\r
#DTEND:%(date)sT160000Z\r
#DESCRIPTION:%(desc)s\r
#BEGIN:VALARM\r
#ACTION:\r
#TRIGGER;VALUE=DURATION:PT0S\r
#END:VALARM\r
#END:VEVENT\r
#"""
#event_tpl = """\r
#BEGIN:VEVENT\r
#UID:ANNIV-%(uid)s\r
#SUMMARY:Anniv. %(name)s\r
#CATEGORIES:Anniversaire\r
#DESCRIPTION:%(desc)s\r
#DTSTART:%(date)sT060000Z\r
#DTEND:%(date)sT160000Z\r
#END:VEVENT\r
#"""
event_dict = {
    "UID": "ANNIV-%(cleanname)s",
    "SUMMARY": "Anniv. %(name)s",
    "CATEGORIES": "Anniversaire",
    "DESCRIPTION": "%(desc)s",
#    "DTSTART": "%(date)sT060000Z",
#    "DTEND": "%(date)sT160000Z",
    "DTSTART;VALUE=DATE": "%(date)s",
    "DTEND;VALUE=DATE": "%(day_after)s",
    "LAST-MODIFIED": "%(mtime)s",
    "DTSTAMP": "%(mtime)s",
    "CREATED": "%(mtime)s",
}
#RRULE:FREQ=YEARLY
#CREATED:%(now)s\r
#LAST-MODIFIED:%(now)s\r
#DTSTAMP:%(now)s\r


class UTC(datetime.tzinfo):
    def utcoffset(self, dt):
        return datetime.timedelta(0)
    def tzname(self, dt):
        return "UTC"
    def dst(self, dt):
        return datetime.timedelta(0)

re_datetime = re.compile("(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)(Z)?")
re_date = re.compile("(\d\d\d\d)-(\d\d)-(\d\d)")
def stringToDateTime(s, tzinfo=None):
    #Returns datetime.datetime object.
    if re_datetime.match(s):
        mo = re_datetime.match(s)
        year = int(mo.group(1))
        month = int(mo.group(2))
        day = int(mo.group(3))
        hour = int(mo.group(4))
        minute = int(mo.group(5))
        second = int(mo.group(6))
        if mo.group(7):
            tzinfo = UTC()
        return datetime.datetime(year, month, day, hour, minute, second, 0, tzinfo)
    elif re_date.match(s):
        mo = re_date.match(s)
        year = int(mo.group(1))
        month = int(mo.group(2))
        day = int(mo.group(3))
        return datetime.datetime(year, month, day, 0, 0, 0, 0, tzinfo)
    else:
        print s
        raise ValueError("Can't parse the date: %s" % s)



def makeICS(vcf_in):
    # Create the calendar
    cal = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\n\r\n"
    events = getICS(vcf_in)
    cal += "\r\n\r\n".join(events)
    cal += "\r\n\r\nEND:VCALENDAR\r\n"
    return cal

def getEvents(vcf_in):
    # Filter out unparsable lines
    addresses = ""
    for line in vcf_in:
        if line.startswith("X-messaging"):
            continue
        addresses += line

    # Create the calendar
    today = datetime.date.today()
    #now = datetime.datetime.utcfromtimestamp(time.mktime(time.localtime())) # timezone-aware
    now = datetime.datetime.now() - datetime.timedelta(hours=2)
    items = []
    for address in vobject.readComponents(addresses):
        name = address.fn.value
        #uid = address.uid.value
        bday = None
        for child in address.getChildren():
            if child.name == "BDAY":
                bday = stringToDateTime(child.value).date()
            elif child.name == "NICKNAME":
                name = child.value
        if bday is None:
            continue
        bday_now = bday.replace(year=today.year)
        day_after = bday_now + datetime.timedelta(days=1)
        desc = "%s - %s years old" % (bday.year, today.year-bday.year)
        if hasattr(address, "rev"):
            mtime = address.rev.value.replace("-","").replace(":","")
        else:
            mtime = now.strftime("%Y%m%dT%H%M%SZ")
        event = {}
        for t_key, t_value in event_dict.iteritems():
            e_value = t_value % {#"uid": uid, 
                               "name": name,
                               "cleanname": re.sub("[^\w]", "", name),
                               "date": bday_now.strftime("%Y%m%d"), 
                               "day_after": day_after.strftime("%Y%m%d"),
                               "desc": desc,
                               "now": now.strftime("%Y%m%dT%H%M%SZ"),
                               "mtime": mtime,
                               }
            event[t_key] = e_value.encode("utf-8")
        items.append(event)
    return items

def getICS(vcf_in):
    events = getEvents(vcf_in)
    items = []
    for event in events:
        item = ["BEGIN:VEVENT"]
        item.extend( [ "%s:%s" % (key, value) for key, value in event.iteritems() ] )
        item.append("END:VEVENT")
        items.append("\r\n".join(item))
    return items


if __name__ == "__main__":
    #addresses_path = "%s/.kde/share/apps/kabc/std.vcf"%os.getenv("HOME")
    addresses_path = sys.argv[1]
    #anniv_path = "/tmp/annivs.ics"
    anniv_path = sys.argv[2]
    addresses_file = open(addresses_path)
    cal = makeICS(addresses_file)
    addresses_file.close()
    annivs = open(anniv_path, "w")
    annivs.write(cal)
    annivs.close()

