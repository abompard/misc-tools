#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:
u"""

Camera-encode
-------------

Re-encode videos taken with my camera from MJPEG to h264.

.. :Authors:
       Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
       GNU GPL v3 or later
"""


INPUT = object()
OUTPUT = object()
COMMAND = ["mencoder", "-oac", "mp3lame", "-srate", "11025", "-ovc", "xvid",
           "-xvidencopts", "bitrate=800", "-mc", "0", INPUT, "-o", OUTPUT]
COMMAND = ["ffmpeg", "-y", "-i", INPUT, "-c:v", "libx264", "-b:v", "800k",
           "-c:a", "libmp3lame", "-b:a", "128k", "-ar", "44100",
           "-map_metadata", "0:g", OUTPUT]
FORMAT = "mp4"

import os
import sys
import json
from glob import glob
from subprocess import call, Popen, PIPE
from tempfile import mkstemp
from optparse import OptionParser


def encode_video(filename, opts):
    f, tmppath = mkstemp(suffix="."+FORMAT, dir=".")
    os.close(f)
    cmd = []
    for c in COMMAND:
        if c == INPUT:
            cmd.append(filename)
        elif c == OUTPUT:
            cmd.append(tmppath)
        else:
            cmd.append(c)
    print " ".join(cmd)
    if opts.dry_run:
        os.remove(tmppath)
        return
    retcode = call(cmd, env=os.environ)
    if retcode == 0:
        if opts.keep:
            os.rename(filename, "%s.orig" % filename)
        else:
            os.remove(filename)
        os.rename(tmppath, filename.rpartition(".")[0] + "." + FORMAT)
    else:
        print "FAILED! Return code was", retcode
        os.remove(tmppath)


def is_already_encoded(filename):
    cmd = Popen(["ffprobe", "-show_format", "-of", "json", filename],
                stdout=PIPE, stderr=PIPE, env=os.environ)
    output = cmd.communicate()[0]
    data = json.loads(output)
    encoder = data["format"]["tags"]["encoder"].strip()
    return encoder.startswith("MEncoder") or encoder.startswith("Lavf")


def parse_opts():
    parser = OptionParser()
    parser.add_option("-g", "--gamma", help="Fix the brightness using a "
                      "gamma correction, if the movies were shot in the dark")
    parser.add_option("-k", "--keep", action="store_true",
                      help="Keep the original file (rename it to .orig)")
    parser.add_option("-n", "--dry-run", action="store_true",
                      help="Don't do anything, just print the commands "
                           "that would be run")
    opts, args =  parser.parse_args()
    for arg in args:
        if not os.path.exists(arg):
            parser.error("%s: no such file" % arg)
    return opts, args


def main():
    opts, to_encode = parse_opts()
    if not to_encode:
        to_encode = glob("*.avi") + glob("*.AVI")
    if opts.gamma:
        #COMMAND[-1:-1] = ["-vf", "eq2=%s" % opts.gamma] # mencoder
        COMMAND[-1:-1] = ["-filter:v", "mp=eq2=%s" % opts.gamma] # ffmpeg
        # Rotation: ["-filter:v", "rotate=PI/2:ow=ih:oh=iw:c=none"]
    for filename in to_encode:
        if is_already_encoded(filename):
            print filename, "is already encoded"
            continue
        encode_video(filename, opts)


if __name__ == "__main__": main()
