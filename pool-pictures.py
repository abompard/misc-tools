#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script de génération d'un 'pool' d'images à partir des images de plusieurs répertoires.
Il est aussi possible d'utiliser un tag de la base de données Digikam
"""

import os, sys, sqlite3
import shutil
from optparse import OptionParser


DIGIKAM_DB_FN = "digikam4.db"


def parse_args():
    description = u"Script de génération d'un \"pool\" d'images."
    parser = OptionParser(description=description)
    parser.add_option("-o", "--output",
                      help="dossier contenant le \"pool\" d'images")
    parser.add_option("-e", "--exclude", action="append",
                      help=u"dossiers à exclure")
    parser.add_option("-t", "--tag", help=u"tag Digikam à sélectionner")
    parser.add_option("-d", "--debug", action="store_true",
                      help=u"mode déboguage")
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error(u"il faut au moins un dossier à scanner")
    for d in args:
        if not os.path.isdir(d):
            parser.error(u"le dossier %s n'est pas valide" % d)
    return opts, args


def scan_dir(basedir, opts):
    images = []
    for root, dirs, files in os.walk(basedir):
        for f in files:
            if f.startswith("."):
                continue
            if f[f.rfind(".")+1:].lower() not in ["jpg", "jpeg", "png"]:
                if opts.debug:
                    print "Exclusion de %s: mauvaise extention" \
                            % os.path.join(root, f)
                continue
            images.append(os.path.join(root, f))
            for e_dir in opts.exclude:
                if e_dir in dirs:
                    if opts.debug:
                        print "Exclusion de %s" % os.path.join(root, e_dir)
                    dirs.remove(e_dir)
    return images


def scan_digikam_dir(basedir, opts):
    images = []
    digikam_db = os.path.join(basedir, DIGIKAM_DB_FN)
    db_conn = sqlite3.connect(digikam_db)
    db_c = db_conn.cursor()
    query = """SELECT a.relativePath, i.name
    FROM Albums AS a
    LEFT JOIN Images AS i ON a.id=i.album
    """
    if opts.tag:
        query += """
    LEFT JOIN ImageTags as it ON it.imageid=i.id
    LEFT JOIN Tags as t ON it.tagid=t.id
    WHERE t.name = "%s";""" % opts.tag
    files = db_c.execute(query)
    for line in files:
        relpath = line[0].encode("utf-8")
        filename = line[1].encode("utf-8")
        relpath = relpath.lstrip("/")
        if relpath.split("/")[0] in opts.exclude:
            if opts.debug:
                print "Exclusion de %s" % os.path.join(
                        basedir, relpath, filename)
            continue
        images.append(os.path.join(basedir, relpath, filename))
    db_c.close()
    return images


def make_links(opts, targets):
    # Nettoyage de l'existant
    if not opts.debug:
        if os.path.exists(opts.output):
            for old_link in os.listdir(opts.output):
                path = os.path.join(opts.output, old_link)
                if os.path.isdir(path):
                    shutil.rmtree(path)
                else:
                    os.remove(path)
        else:
            os.mkdir(opts.output, 0775)
    # Création des liens
    for target in targets:
        subdir = os.path.join(os.path.basename(target)[0],
                              os.path.basename(target)[1])
        try:
            os.makedirs(os.path.join(opts.output, subdir))
        except OSError:
            pass
        linkfilename = os.path.join(opts.output, subdir, os.path.basename(target))
        if not os.path.isfile(target):
            if not opts.debug:
                print # à cause du "point de progression"
            print "Attention : \"%s\" n'existe pas, je saute" % target
            continue
        if os.path.exists(linkfilename):
            if not opts.debug:
                print # à cause du "point de progression"
            print "Attention : \"%s\" existe déjà, je saute" % linkfilename
            continue
        if opts.debug:
            print 'ln -s "%s" "%s"' % (target, linkfilename)
            continue
        os.symlink(target, linkfilename)
        sys.stdout.write(".")
        sys.stdout.flush()


def main():
    opts, directories = parse_args()
    targets = []
    for directory in directories:
        if os.path.exists(os.path.join(directory, DIGIKAM_DB_FN)):
            targets.extend(scan_digikam_dir(directory, opts))
        else:
            targets.extend(scan_dir(directory, opts))
    make_links(opts, targets)
    print


if __name__=='__main__':
    main()
